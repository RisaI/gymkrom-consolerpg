﻿using System;
using System.Collections.Generic;

namespace gymkromconsolerpg
{
	public class GameWorld
	{
		/// <summary>
		/// Vrati sirku herniho sveta.
		/// </summary>
		/// <value>Sirka.</value>
		public int Width {
			get;
			private set; //zajisti, ze je sirka nastavitelna pouze uvnitr tridy HerniSvet
		}

		/// <summary>
		/// Vrati vysku herniho sveta.
		/// </summary>
		/// <value>Vyska.</value>
		public int Height {
			get {
				return Tiles.Length / Width;
			}
		}

		/// <summary>
		/// Mapa bloku
		/// </summary>
		/// <value>Mapa bloku.</value>
		public byte[] Tiles {
			get;
			private set;
		}

		public List<Entity> Entities {
			get;
			private set;
		}

		public Player Player {
			get;
			private set;
		}

		/// <summary>
		/// Hlavni konstruktor.
		/// </summary>
		/// <param name="vyska">Vyska.</param>
		/// <param name="sirka">Sirka.</param>
		public GameWorld (int width, int height)
		{
			Width = width;
			Tiles = new byte[Width * height];
			Tiles [1] = 1;
			Tiles [3] = 1;
			Tiles [5] = 1;
			Entities = new List<Entity> ();
			Entities.Add (Player = new Player(this));
			Entities.Add (new Enemy(this){ Y = 5 });
		}

		public void PlayerInput(ConsoleKeyInfo key)
		{
			Entities.ForEach (e => e.PlayerInput(key));
			Entities.RemoveAll (e => {
				return e.HP <= 0;
			});
		}

		/// <summary>
		/// Vykresli svet do konzole.
		/// </summary>
		public void Draw()
		{
			Console.Clear ();
			for (int y = 0; y < Height; ++y) {
				Console.SetCursorPosition (0, y);
				for (int x = 0; x < Width; ++x) {
					TileOnCoords(x, y).Draw();
				}
			}
			Entities.ForEach (e => { e.Draw(); });
			Console.ResetColor ();
		}

		public void DrawTile(int x, int y)
		{
			TileOnCoords(x, y).Draw (x, y);
		}

		public Tile TileOnCoords(int x, int y)
		{
			return Tile.TileRegister[Tiles[y * Width + x]];
		}

		public Entity EntityOnCoords(int x, int y)
		{
			return Entities.Find(e => { return e.X == x && e.Y == y && e.HP > 0; });
		}
	}
}

