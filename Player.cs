﻿using System;

namespace gymkromconsolerpg
{
	public class Player : Entity
	{
		public Player (GameWorld world) : base(world, "Hráč", 'P', ConsoleColor.Blue)
		{
			HP = MaxHP = 100;
		}

		public override void Interaction (Entity ent)
		{
			if (ent is Enemy) {
				BattleScreen.BeginBattle (this, ent as Enemy);
			}
		}

		public override void PlayerInput (ConsoleKeyInfo key)
		{
			if (key.Key == ConsoleKey.W)
				MoveBy (0, -1);
			if (key.Key == ConsoleKey.S)
				MoveBy (0, 1);
			if (key.Key == ConsoleKey.A)
				MoveBy (-1, 0);
			if (key.Key == ConsoleKey.D)
				MoveBy (1, 0);
		}
	}
}

