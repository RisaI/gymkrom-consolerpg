﻿using System;

namespace gymkromconsolerpg
{
	public abstract class Entity
	{
		public int X {
			get;
			set;
		}

		public int Y {
			get;
			set;
		}

		public string Name {
			get;
			private set;
		}

		public char Texture {
			get;
			private set;
		}

		public ConsoleColor Color
		{
			get;
			private set;
		}

		public GameWorld World {
			get;
			private set;
		}

		private int _hp = 10;
		public int HP {
			get {
				return _hp;
			}
			set {
				_hp = Math.Min (MaxHP, value);
				if (_hp <= 0)
					Death ();
			}
		}
		private int _maxhp = 10;
		public int MaxHP
		{
			get {
				return _maxhp;
			}
			set {
				if (value < _hp)
					_hp = _maxhp = value;
				else
					_maxhp = value;
			}
		}

		public Entity (GameWorld world, string name, char texture = 'E', ConsoleColor color = ConsoleColor.Red)
		{
			World = world;
			Texture = texture;
			Color = color;
			Name = name;
		}

		public abstract void Interaction (Entity ent);
		public abstract void PlayerInput (ConsoleKeyInfo key);
		public virtual void Death()
		{
			World.DrawTile (X, Y);
		}

		public virtual void MoveBy(int x, int y)
		{
			MoveTo (X + x, Y + y);
		}

		public virtual void MoveTo(int x, int y)
		{
			if (x >= 0 && y >= 0 && x < World.Width && y < World.Height && !World.TileOnCoords (x, y).Collision) {
				var ent = World.EntityOnCoords (x, y);
				if (ent != null) {
					Interaction (ent);
				} else {
					World.DrawTile (X, Y);
					X = x;
					Y = y;
					Draw ();
				}
			}
		}

		public void Draw()
		{
			if (HP <= 0)
				return;

			Console.SetCursorPosition (X, Y);
			Console.ForegroundColor = Color;
			Console.BackgroundColor = ConsoleColor.White;
			Console.Write (Texture);
			Console.ResetColor ();
		}
	}
}

