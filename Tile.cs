﻿using System;

namespace gymkromconsolerpg
{
	public class Tile
	{
		/// <summary>
		/// Registr existujicich typu bloku
		/// </summary>
		public static Tile[] TileRegister = new Tile[]
		{
			new Tile(',', ConsoleColor.Green), //Trava (0)
			new Tile('O', ConsoleColor.Gray, true) //Kamen (1)
		};

		/// <summary>
		/// Symbol slouzici k vykresleni bloku
		/// </summary>
		/// <value>Textura</value>
		public char Texture {
			get;
			private set;
		}

		/// <summary>
		/// Barva
		/// </summary>
		/// <value>The barva.</value>
		public ConsoleColor Color {
			get;
			private set;
		}


		/// <summary>
		/// Pokud ano, na jim zabrany blok se neda vstoupit
		/// </summary>
		/// <value><c>true</c> if blokujici pohyb; otherwise, <c>false</c>.</value>
		public bool Collision {
			get;
			private set;
		}

		/// <summary>
		/// Hlavni konstruktor
		/// </summary>
		/// <param name="textura">Textura.</param>
		/// <param name="barva">Barva.</param>
		public Tile (char texture, ConsoleColor color = ConsoleColor.White, bool collision = false)
		{
			Texture = texture;
			Color = color;
			Collision = collision;
		}

		public void Draw(int x, int y)
		{
			Console.SetCursorPosition (x, y);
			Draw ();
		}

		public void Draw()
		{
			Console.ForegroundColor = Color;
			Console.Write (Texture);
		}
	}
}

