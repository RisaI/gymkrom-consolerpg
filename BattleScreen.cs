﻿using System;

namespace gymkromconsolerpg
{
	public static class BattleScreen
	{
		const byte BUTTON_WIDTH = 16, BUTTON_COUNT = 3;
		static byte SelectedButton;
		static bool Ongoing;

		public static void BeginBattle(Player player, Enemy enemy)
		{
			SelectedButton = 0;
			Ongoing = true;
			while (Ongoing) {
				Draw (player, enemy);

				var tlacitko = Console.ReadKey (true);

				switch (tlacitko.Key) {
				case ConsoleKey.W:
					if (SelectedButton == 0)
						SelectedButton = BUTTON_COUNT - 1;
					else
						--SelectedButton;
					break;
				case ConsoleKey.S:
					if (SelectedButton == BUTTON_COUNT - 1)
						SelectedButton = 0;
					else
						++SelectedButton;
					break;
				case ConsoleKey.Enter:
					Action (player, enemy);
					break;
				case ConsoleKey.Escape:
					Ongoing = false;
					break;
				}

				if (player.HP <= 0) {

				}
				if (enemy.HP <= 0) {
					break;
				}
			}
			player.World.Draw ();
		}

		static void Draw(Player player, Entity enemy)
		{
			Console.Clear ();

			//hrac
			{
				Console.SetCursorPosition (0, 0);
				Console.Write (player.Name);
				Console.SetCursorPosition (0, 1);
				Console.Write ("{0}/{1} HP", player.HP, player.MaxHP);
			}

			//nepritel
			{
				Console.SetCursorPosition (Console.BufferWidth - enemy.Name.Length, 0);
				Console.Write (enemy.Name);
				var hpText = enemy.HP + "/" + enemy.MaxHP + " HP";
				Console.SetCursorPosition (Console.BufferWidth - hpText.Length, 1);
				Console.Write (hpText);
			}

			//tlacitka
			{
				CurrentButton = 0;
				Console.SetCursorPosition (2, 16);
				Button ("Útok");
				Console.SetCursorPosition (2, 17);
				Button ("Obrana");
				Console.SetCursorPosition (2, 18);
				Button ("Útěk");
			}
		}

		static int CurrentButton;
		static void Button(string text)
		{
			Console.ForegroundColor = CurrentButton == SelectedButton ? ConsoleColor.Black : ConsoleColor.White;
			Console.BackgroundColor = CurrentButton++ == SelectedButton ? ConsoleColor.White : ConsoleColor.Black;
			Console.Write (' ');
			Console.Write (text.Length > BUTTON_WIDTH - 2? text.Substring (BUTTON_WIDTH - 2) : text);
			for (int i = 0; i < BUTTON_WIDTH - text.Length - 2; ++i)
				Console.Write (' ');
			Console.Write (' ');
			Console.ResetColor ();
		}

		static void Action(Player player, Enemy enemy)
		{
			switch (SelectedButton) {
			case 0:
				--enemy.HP;
				break;
			case 2:
				Ongoing = false;
				return;
			}
		}
	}
}

